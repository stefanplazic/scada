﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScadaLibrary;

namespace DatabaseManager
{
    public partial class DoForm : Form
    {
        private DO doTag;

        public DO DoTag
        {
            get { return doTag; }
            set { doTag = value; }
        }

        public DoForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try {
                String id = textBox1.Text;
                String description = textBox2.Text;
                String address = textBox4.Text;
                String driverType = (String)comboBox1.SelectedItem;

                SimulationDriver driver = new SimulationDriver();
                switch (driverType)
                {
                    case "Sine":
                        driver.TypeSignal = TypeSignal.Sine;
                        break;
                    case "Cosine":
                        driver.TypeSignal = TypeSignal.Cosine;
                        break;
                    case "Triangle":
                        driver.TypeSignal = TypeSignal.Triangle;
                        break;
                    case "Ramp":
                        driver.TypeSignal = TypeSignal.Ramp;
                        break;
                    case "Rectangle":
                        driver.TypeSignal = TypeSignal.Rectangle;
                        break;
                    default:
                        driver.TypeSignal = TypeSignal.Sine;
                        break;
                }
                double initValue = double.Parse(textBox3.Text);

                this.doTag = new DO(id, description, driver, address, initValue);
            

            }
            catch (Exception exp)
            {
                MessageBox.Show("Wrong inputs!");
                this.DialogResult = DialogResult.Retry;
            }
            


        }
    }
}
