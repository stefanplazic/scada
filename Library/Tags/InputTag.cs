﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ScadaLibrary.Tags
{
    
    [DataContract]
    
    public abstract class InputTag:Tag
    {

        [DataMember]
        private int scanTime;
        [DataMember]
        private bool scan;
        [DataMember]
        private bool auto;
        [DataMember]
        private Alarm alarm;

        public Alarm Alarm
        {
            get { return alarm; }
            set { alarm = value; }
        }
        

        public InputTag(string Id, string Description, SimulationDriver Sd, 
            string Address, int ScanTime, bool Scan, bool Auto, Alarm alarm): base(Id,Description,Sd,Address)
        {
            scanTime = ScanTime;
            scan = Scan;
            auto = Auto;
            this.alarm = alarm;
        }

        public InputTag() { }

        public bool Auto
        {
            get { return auto; }
            set { auto = value; }
        }

        public bool Scan
        {
            get { return scan; }
            set { scan = value; }
        }

        public int ScanTime
        {
            get { return scanTime; }
            set { scanTime = value; }
        }

        public abstract double calculateValue();
    }
}
