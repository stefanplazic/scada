﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ServiceModel;
using ScadaLibrary;
using ScadaLibrary.Interfaces;

namespace AlarmClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.Sleep(3000);
            Uri addr = new Uri("net.tcp://localhost:8085/IAlarm");
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            ChannelFactory<IAlarm> fact = new ChannelFactory<IAlarm>(binding, new EndpointAddress(addr));

            IAlarm proxy = fact.CreateChannel();
            Console.WriteLine("Alarms:");

            Thread t = new Thread(new ParameterizedThreadStart(writeAlarms));
            t.IsBackground = true;
            t.Start(proxy);
                        
            Console.ReadKey();

        }

        public static void writeAlarms(object prox)
        {

            IAlarm proxy = (IAlarm)prox;
            //needs for comparing previus with current states of AlarmType
            Dictionary<string, AlarmType> prev_types = new Dictionary<string, AlarmType>();
            while (true)
            {
                Dictionary<string, Alarm> alarms = proxy.getAlarms();
                foreach (var item in alarms)
                {
                    string id = item.Key;
                    Alarm alarm = item.Value;
                    if (prev_types.ContainsKey(id))
                    {
                        if (prev_types[id] != alarm.AlarmType)
                        {
                            Console.WriteLine("ID: {"+id + "}  Date: {" + alarm.Date + "} , Type: {" + alarm.AlarmType + "}");
                            //set prev AlaramType to current 
                            prev_types[id] = alarm.AlarmType;
                        }
                    }
                    else
                    { 
                        //add to dictionary and write it to console
                        prev_types.Add(id,alarm.AlarmType);
                        Console.WriteLine("ID: {" + id + "}  Date: {" + alarm.Date + "} , Type: {" + alarm.AlarmType + "}");
                    }
                    
                }
                Thread.Sleep(500);
            }
        }
    }
}
