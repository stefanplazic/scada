﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScadaLibrary.Tags;
using System.Runtime.Serialization;

namespace ScadaLibrary
{
    [DataContract]
    public class AO:OutputTag
    {
        [DataMember]
        private double lowLimit;
        [DataMember]
        private double highLimit;
        [DataMember]
        private String units;

        public String Units
        {
            get { return units; }
            set { units = value; }
        }

        public double HighLimit
        {
            get { return highLimit; }
            set { highLimit = value; }
        }

        public double LowLimit
        {
            get { return lowLimit; }
            set { lowLimit = value; }
        }
        public AO() { }

        public AO(string Id, string Description, SimulationDriver Sd, string Address, double initValue,double lowLimit,double highLimit,string units) 
            : base(Id, Description, Sd, Address, initValue) {
                this.lowLimit = lowLimit;
                this.highLimit = highLimit;
                this.units = units;
            
        }


        
    }
}
