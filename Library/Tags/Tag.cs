﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ScadaLibrary.Tags
{
    
    [DataContract]
    [KnownType(typeof(SimulationDriver))]
    [KnownType(typeof(Alarm))]
    [KnownType(typeof(DO))]
    [KnownType(typeof(InputTag))]
    [KnownType(typeof(OutputTag))]

    [XmlInclude(typeof(AI))]
    [XmlInclude(typeof(AO))]
    [XmlInclude(typeof(DI))]
    [XmlInclude(typeof(DO))]
    [XmlInclude(typeof(InputTag))]
    [XmlInclude(typeof(OutputTag))]
    public abstract class Tag
    {
        [DataMember]
        private string id;
        [DataMember]
        private string description;
        [DataMember]
        private SimulationDriver driver;
        [DataMember]
        private string address;
        

        public Tag(string Id, string Description, SimulationDriver Sd, string Address)
        {
            
            id = Id;
            description = Description;
            driver = Sd;
            address = Address;
           
        }

        public Tag() { }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public SimulationDriver Driver
        {
            get { return driver; }
            set { driver = value; }
        }

        public string Adress
        {
            get { return address; }
            set { address = value; }
        }

    }
}
