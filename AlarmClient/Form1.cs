﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScadaLibrary.Interfaces;
using ScadaLibrary;
using System.Threading;

namespace AlarmClient
{
    public partial class Form1 : Form
    {
        private IAlarm proxy;
        private Thread thread;
        private DataTable table;

        public Form1(IAlarm proxy)
        {
            InitializeComponent();
            this.proxy = proxy;
             this.table = new DataTable();
            table.Columns.Add("Id");
            table.Columns.Add("Date");
            table.Columns.Add("Alarm Type");

            dataGridView1.DataSource = table;

           /* Dictionary<string, Alarm> alarms = proxy.getAlarms();
            for (int i = 0; i < alarms.Count; i++)
            {
                var item = alarms.ElementAt(i);
                string id = item.Key;
                Alarm alarm = item.Value;


                DataRow row = table.NewRow();

                row["Id"] = id;
                row["Date"] = alarm.Date;
                row["Alarm Type"] = alarm.AlarmType;
                table.Rows.Add(row);


            }*/
                
            
            
            
            
            thread = new Thread(new ThreadStart(showAlarms));
            thread.IsBackground = true;
            thread.Start();
        }


        public void showAlarms(){
            while(true){
            Dictionary<string, Alarm> alarms = proxy.getAlarms();
            for (int i = 0; i < alarms.Count; i++)
            {
                var item = alarms.ElementAt(i);
                string id = item.Key;
                Alarm alarm = item.Value;





                DataRow row = this.table.NewRow();
                row["Id"] = id;
                row["Date"] = alarm.Date;
                row["Alarm Type"] = alarm.AlarmType;
                table.Rows.Add(row);

                // dataGridView1.DataSource = table;
                Thread.Sleep(500);
            }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
