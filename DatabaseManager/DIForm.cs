﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScadaLibrary;

namespace DatabaseManager
{
    public partial class DIForm : Form
    {
        private DI diTag;

        public DI DiTag
        {
            get { return diTag; }
            set { diTag = value; }
        }
        public DIForm()
        {
            InitializeComponent();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            try {
                String id = textBox1.Text;

                String description = textBox2.Text;
                String address = textBox4.Text;
                string driverType = (string) comboBox1.SelectedItem;

                SimulationDriver driver = new SimulationDriver();
                
                switch (driverType)
                {
                    case "Sine":
                        driver.TypeSignal = TypeSignal.Sine;
                        break;
                    case "Cosine":
                        driver.TypeSignal = TypeSignal.Cosine;
                        break;
                    case "Triangle":
                        driver.TypeSignal = TypeSignal.Triangle;
                        break;
                    case "Ramp":
                        driver.TypeSignal = TypeSignal.Ramp;
                        break;
                    case "Rectangle":
                        driver.TypeSignal = TypeSignal.Rectangle;
                        break;
                    default:
                        driver.TypeSignal = TypeSignal.Sine;
                        break;
                }
                int scanTime = int.Parse(textBox4.Text);//try and catch 
                bool turnOnScan = checkBox1.Checked;
                bool automatic = checkBox2.Checked;

                double ll = double.Parse(textBox5.Text);
                double l = double.Parse(textBox6.Text);
                double h = double.Parse(textBox7.Text);
                double hh = double.Parse(textBox8.Text);

                Alarm alarm = new Alarm(ll, l, h, hh);
                //make Di tag and add all properties
                this.diTag = new DI(id, description, driver, address, scanTime, turnOnScan, automatic, alarm);
            
            
            }
            catch(Exception exp){
                
                MessageBox.Show("Wrong inputs!");
                this.DialogResult = DialogResult.Retry;
            }

            
             
        }

        
    }
}
