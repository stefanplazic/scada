﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScadaLibrary;

namespace DatabaseManager
{
    public partial class AIForm : Form
    {
        private AI aiTag;

        public AI AiTag
        {
            get { return aiTag; }
            set { aiTag = value; }
        }
        public AIForm()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                String id = textBox1.Text;
                String description = textBox2.Text;
                String address = textBox3.Text;
                String driverType = (string) comboBox1.SelectedItem;

                SimulationDriver driver = new SimulationDriver();
                switch (driverType)
                {
                    case "Sine":
                        driver.TypeSignal = TypeSignal.Sine;
                        break;
                    case "Cosine":
                        driver.TypeSignal = TypeSignal.Cosine;
                        break;
                    case "Triangle":
                        driver.TypeSignal = TypeSignal.Triangle;
                        break;
                    case "Ramp":
                        driver.TypeSignal = TypeSignal.Ramp;
                        break;
                    case "Rectangle":
                        driver.TypeSignal = TypeSignal.Rectangle;
                        break;
                    default:
                        driver.TypeSignal = TypeSignal.Sine;
                        break;
                }
                int scanTime = int.Parse(textBox4.Text);//try and catch 
                bool turnOnScan = checkBox1.Checked;
                bool automatic = checkBox2.Checked;

                /*limits and units*/
                double lowLimit = Double.Parse(textBox9.Text);
                double highLimit = Double.Parse(textBox10.Text);
                
                String units = textBox11.Text;

                double ll = double.Parse(textBox5.Text);
                double l = double.Parse(textBox6.Text);
                double h = double.Parse(textBox7.Text);
                double hh = double.Parse(textBox8.Text);

                Alarm alarm = new Alarm(ll, l, h, hh);
                //make AI tag
                this.aiTag = new AI(id, description, driver, address, scanTime, turnOnScan, automatic, lowLimit, highLimit, units, alarm);
                
                

            }catch(Exception exp){
                MessageBox.Show("Wrong inputs!");
                this.DialogResult = DialogResult.Retry;
            }

            
        }

        


        

       
    }
}
