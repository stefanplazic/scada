﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.ServiceModel;
using ScadaLibrary.Interfaces;

namespace TrendingClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*proxy configuration*/
            Thread.Sleep(2000);
            Uri addr = new Uri("net.tcp://localhost:8082/ITrending");
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            ChannelFactory<ITrending> fact = new ChannelFactory<ITrending>(binding, new EndpointAddress(addr));

            ITrending proxy = fact.CreateChannel();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(proxy));
        }
    }
}
