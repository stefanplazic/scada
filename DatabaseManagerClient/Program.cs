﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ScadaLibrary.Interfaces;
using ScadaLibrary.Tags;
using ScadaLibrary;
using System.Threading;

namespace DatabaseManagerClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.Sleep(2000);
            Uri addr = new Uri("net.tcp://localhost:8083/IDatabaseManager");
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            ChannelFactory<IDatabaseManager> fact = new ChannelFactory<IDatabaseManager>(binding, new EndpointAddress(addr));

            IDatabaseManager proxy = fact.CreateChannel();

            Console.WriteLine("Pristupili ste serveru...");
            DI tag = new DI("1","",new SimulationDriver(),"",1,true,true);
            
            proxy.addTag(tag);
            Console.WriteLine("Dodali ste tag br " + tag.Id);

           

            Alarm al = new Alarm();
            al.High=80;
            al.HighHigh=90;
            al.Low=20;
            al.LowLow=10;

            proxy.addAlarm("1",al);
            

            /**********************ADD NEW TAG**********************************/
            Thread.Sleep(2000);
            DI tag2 = new DI("2", "", new SimulationDriver(), "", 1, true, true);

            proxy.addTag(tag2);
            Console.WriteLine("Dodali ste tag br " + tag2.Id);



            Alarm al2 = new Alarm();
            al2.High = 100;
            al2.HighHigh = 90;
            al2.Low = 30;
            al2.LowLow = 10;

            proxy.addAlarm(tag2.Id, al2);
            

            Console.ReadKey();
        }
    }
}
