﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.ServiceModel;
using ScadaLibrary.Interfaces;

namespace AlarmClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //proxy
            Thread.Sleep(3000);
            Uri addr = new Uri("net.tcp://localhost:8085/IAlarm");
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;

            ChannelFactory<IAlarm> fact = new ChannelFactory<IAlarm>(binding, new EndpointAddress(addr));

            IAlarm proxy = fact.CreateChannel();
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(proxy));
        }
    }
}
