﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ScadaLibrary
{
    public enum AlarmType {LowLow, Low, High, HighHigh, None}

    [DataContract]
    public class Alarm
    {
        [DataMember]
        private DateTime date;
        [DataMember]
        private double lowLow;
        [DataMember]
        private double low;
        [DataMember]
        private double high;
        [DataMember]
        private double highHigh;
        [DataMember]
        private AlarmType alarmType = AlarmType.None;

        public Alarm() { }

        public Alarm(Double ll, double l, double h, double hh) {
            lowLow = ll;
            low = l;
            high = h;
            highHigh = hh;
        }
        /*dodavanje novog stanja*/

        

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }


        public AlarmType AlarmType
        {
            get { return alarmType; }
            set { alarmType = value; }
        }

        

        public double HighHigh
        {
            get { return highHigh; }
            set { highHigh = value; }
        }

        public double High
        {
            get { return high; }
            set { high = value; }
        }

        public double Low
        {
            get { return low; }
            set { low = value; }
        }

        public double LowLow
        {
            get { return lowLow; }
            set { lowLow = value; }
        }

        public AlarmType setAlarmType( double value) {
            if (value < lowLow)
                alarmType = AlarmType.LowLow;
            else if (value < low)
                alarmType = AlarmType.Low;
            else if (value > highHigh)
                alarmType = AlarmType.HighHigh;
            else if (value > high)
                alarmType = AlarmType.High;
            return alarmType;
        }
    }
}
