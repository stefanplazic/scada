﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ScadaLibrary.Interfaces;
using System.Threading;

namespace ScadaServer
{
    class Program
    {

        private static Server server;
        private static ServiceHost service;

        public static void Start() {
            Uri address1 = new Uri("net.tcp://localhost:8083/IDatabaseManager");
            Uri address2 = new Uri("net.tcp://localhost:8085/IAlarm");
            Uri address3 = new Uri("net.tcp://localhost:8082/ITrending");

            server = new Server();
            

            NetTcpBinding binding1 = new NetTcpBinding();
            NetTcpBinding binding2 = new NetTcpBinding();
            NetTcpBinding binding3 = new NetTcpBinding();

            binding1.Security.Mode = SecurityMode.None;
            binding2.Security.Mode = SecurityMode.None;
            binding3.Security.Mode = SecurityMode.None;

            service = new ServiceHost(server);

            service.AddServiceEndpoint(typeof(IDatabaseManager), binding1, address1);
            service.AddServiceEndpoint(typeof(IAlarm), binding2, address2);
            service.AddServiceEndpoint(typeof(ITrending), binding3, address3);
            service.Open();

            Thread.Sleep(1000);
            server.Deserialization();
            Console.WriteLine("Server is ready...");
        }
        public static void Stop() {
            server.Serialization();
            server.file.Close();
            service.Close();
            Console.WriteLine("Server is closed.");
        }

        static void Main(string[] args)
        {
            Start();
            Console.ReadKey();
            Stop();
        }
    }
}
