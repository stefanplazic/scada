﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ScadaLibrary.Interfaces;
using ScadaLibrary.Tags;
using ScadaLibrary;
using System.Threading;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace ScadaServer
{
    public delegate void AlarmDelegate(InputTag alarm);
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Server:IDatabaseManager, ITrending, IAlarm
    {
        Dictionary<String, Tag> tags = new Dictionary<String, Tag>();
        public event AlarmDelegate alarmDelegate;
        public Dictionary<String, Thread> threads = new Dictionary<String, Thread>();
        public StreamWriter file;

        public Server() {

            if (file == null)
            {
                try
                {
                    file = new StreamWriter("alarmLog.txt", true);
                    file.Close();
                }
                finally
                {
                    file.Close();
                }

            }
            else {
                file.Close();
            }
            
            alarmDelegate = new AlarmDelegate(writeFile);
            alarmDelegate += new AlarmDelegate(printLog);

            
        }

        private List<Tag> getListTags() {
            List<Tag> lista = new List<Tag>();

            return lista;
        }
        private void printLog(InputTag tag) {
            Console.WriteLine("ID: {"+tag.Id + "} Date {" + tag.Alarm.Date+"} , Type: {"+tag.Alarm.AlarmType+"}");
        }

        private void writeFile(InputTag tag)
        {
         lock (file){
             try
             {
                 file = new StreamWriter("alarmLog.txt", true);
                 file.WriteLine("ID: {" + tag.Id + "} Date {" + tag.Alarm.Date + "} , Type: {" + tag.Alarm.AlarmType + "}");
                 //file.Close();
             }
             catch {
                 file.Close();
             }
                
            }
            
             
         }
            
                
        

        public Tag addTag(Tag tag)
        {
            if (!tags.ContainsKey(tag.Id)) {
                tags.Add(tag.Id, tag);
                Console.WriteLine("Tag with id: "+tag.Id+" added");

                Thread th = new Thread(new ParameterizedThreadStart(scan));
                th.Start(tag);
                threads[tag.Id] = th;
                return tag;
            }
            return null;
        }

        public void removeTag(string id)
        {
            if (tags.ContainsKey(id))
            {
                tags.Remove(id);
               
                threads[id].Abort();
                threads.Remove(id);
            }
        }


        public Dictionary<string, Tag> getTags()
        {
            return tags;
        }

        public Dictionary<string, Alarm> getAlarms()
        {
            Dictionary<string, Alarm> alarms = new Dictionary<string, Alarm>();
            foreach (Tag item in tags.Values){
                if (item is InputTag) {
                    InputTag tag = (InputTag)item;
                    if (tag.Alarm!=null && tag.Alarm.setAlarmType(tag.calculateValue()) != AlarmType.None)
                    {
                        tag.Alarm.Date = DateTime.Now;
                        alarms.Add(tag.Id, tag.Alarm);
                    }
                        
                }
            }
            return alarms;
        }
        
        //scaning
        public void scan(object input)
        {
            InputTag inp = null;
            if (input is DI)
                inp = (DI)input;

            if (input is AI)
                inp = (AI)input;

            if (inp == null)
                return;
            int milliseconds = inp.ScanTime;
            AlarmType prev = AlarmType.None;
            while (true)
            {
                if (inp.Auto && inp.Scan)
                {
                    double val = inp.calculateValue();
                    AlarmType current =inp.Alarm.setAlarmType(val);
                    if (inp.Alarm != null  &&  current!= AlarmType.None && current!=prev)
                    {
                        prev = current;
                        inp.Alarm.Date = DateTime.Now;
                        onAlarmChanged(inp);
                    }
                    
                }
                Thread.Sleep(milliseconds);
            }
        }

        public void onAlarmChanged(InputTag alarm)
        {
            if (alarmDelegate != null)
            {
                alarmDelegate(alarm);
            }
        }
        //convert tags to list
        public List<Tag> convertToList()
        {
            List<Tag> t ;
            

            t = tags.Values.ToList();
            return t;

        }
        //convert list to dictionary
        public void convertToDic(List<Tag> t)
        {
            foreach (Tag item in t)
            {
                tags.Add(item.Id,item);
            }
        }

        public void Serialization()
        {
            List<Tag> listTags = convertToList();
            XmlSerializer serializer = new XmlSerializer(typeof(List<Tag>));
            using (TextWriter textWriter = new StreamWriter("settings.xml"))
            {
                serializer.Serialize(textWriter, listTags);
            }
           
        }

        public void Deserialization()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Tag>));
            StreamReader sr = new StreamReader("settings.xml");
            try
            {
                List<Tag> t = (List<Tag>)serializer.Deserialize(sr);
                sr.Close();
                foreach (Tag item in t)
                {
                    addTag(item);
                }
            }
            catch {
                sr.Close();
            } 

            
        }

        public Dictionary<string, Tag> getScanTags()
        { 
            lock (tags) {
                Dictionary<String, Tag> scanTags = new Dictionary<String, Tag>();
                foreach (Tag tag in tags.Values)
                {
                    if (tag is InputTag)
                    {
                        InputTag inputTag = (InputTag)tag;
                        if (inputTag.Scan)
                        {
                            scanTags.Add(tag.Id, tag);
                        }
                    }
                }
                return scanTags;
            }
           
        }
    }
}
