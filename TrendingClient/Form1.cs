﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScadaLibrary.Tags;
using ScadaLibrary.Interfaces;
using System.Windows.Forms.DataVisualization.Charting;
using ScadaLibrary;
using System.Threading;

namespace TrendingClient
{
    public partial class Form1 : Form
    {
        
        System.Windows.Forms.Timer MyTimer;

        ITrending proxy;

        List<Color> colors = new List<Color>();
        List<Tag> listTags = new List<Tag>();
        Dictionary<string, Tag> tempTags;
        Dictionary<string, Tag> oldTags;

        public Form1(ITrending p)
        {
            

            proxy = p;

            InitializeComponent();
            chart1.ChartAreas[0].AxisY.Maximum = 55;
            chart1.ChartAreas[0].AxisY.Minimum = -55;
            
            chart1.ChartAreas[0].CursorX.IsUserEnabled = true;
            chart1.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;


            //chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisX.ScrollBar.IsPositionedInside = true;
            chart1.ChartAreas[0].AxisX.ScrollBar.Enabled = true;
            chart1.ChartAreas[0].AxisX.IsLabelAutoFit = true;
            chart1.ChartAreas[0].AxisX.ScaleView.Size = 60;
            chart1.ChartAreas[0].AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll;
            chart1.ChartAreas[0].AxisX.Interval = 5;

            //chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisY.ScrollBar.IsPositionedInside = true;
            chart1.ChartAreas[0].AxisY.ScrollBar.Enabled = true;
            chart1.ChartAreas[0].AxisY.IsLabelAutoFit = true;
            chart1.ChartAreas[0].AxisY.ScaleView.Size = 110;
            chart1.ChartAreas[0].AxisY.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll;
            chart1.ChartAreas[0].AxisY.Interval = 10;

            
            colors.Add(Color.Black);
            colors.Add(Color.Green);
            colors.Add(Color.Blue);
            colors.Add(Color.DeepPink);
            colors.Add(Color.Turquoise);
            colors.Add(Color.Coral);
            colors.Add(Color.DarkViolet);
            colors.Add(Color.MediumBlue);
            colors.Add(Color.Lime);


            tempTags = proxy.getScanTags();
            foreach (Tag tag in tempTags.Values)
                {
                    checkedListBox1.Items.Add(tag.Id + " " + tag.Description + " " + tag.Driver.TypeSignal.ToString());
                }
             
            

            
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void MyTimer_Tick(object sender, EventArgs e)
        {

            for (int i = 0; i < listTags.Count; i++) {
                chart1.Series[listTags[i].Id].Points.Add(((InputTag)listTags[i]).calculateValue());
            }

                oldTags = tempTags;
                tempTags = proxy.getScanTags();
            if (ListChanged(oldTags, tempTags)) {
                checkedListBox1.BeginUpdate();
                checkedListBox1.Items.Clear();
                foreach (Tag tag in tempTags.Values)
                {
                    checkedListBox1.Items.Add(tag.Id + " " + tag.Description + " " + tag.Driver.TypeSignal.ToString());
                }
                checkedListBox1.EndUpdate();
                
            }
            

        }

        private bool ListChanged(Dictionary<string, Tag> oldTags, Dictionary<string, Tag> newTags )
        {
            if (oldTags.Keys.Count != newTags.Keys.Count)
                return true;
            return false;
        }

        private void addChartSeries() {
            lock (chart1) {

                
                chart1.Series.Clear();
                chart1.Legends.Clear();
                for (int i = 0; i < listTags.Count; i++)
                {

                    Series newSeries = new Series();
                    Legend legend = new Legend();

                    newSeries.ChartArea = "ChartArea1";
                    newSeries.Name = listTags[i].Id;
                    newSeries.ChartType = SeriesChartType.Line;
                    newSeries.Color = colors[i];
                    newSeries.BorderWidth = 2;

                    chart1.Series.Add(newSeries);

                    legend.Name = listTags[i].Id;
                    legend.Title = "Tags :";
                    chart1.Legends.Add(legend);

                }
            }
            
            

            
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (MyTimer != null)
                MyTimer.Stop();
         

                lock (tempTags)
                {
                    listTags.Clear();
                    foreach (string itemChecked in checkedListBox1.CheckedItems)
                    {
                        string id = itemChecked.Split(null)[0];
                        listTags.Add(tempTags[id]);

                    }

                }

            addChartSeries();

            MyTimer= new System.Windows.Forms.Timer();
            MyTimer.Interval = (1000);
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            MyTimer.Start();


        }

      
    }
}
