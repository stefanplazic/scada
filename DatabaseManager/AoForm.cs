﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScadaLibrary;

namespace DatabaseManager
{
    public partial class AoForm : Form
    {
        private AO aoTag;

        public AO AoTag
        {
            get { return aoTag; }
            set { aoTag = value; }
        }
        public AoForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try{
                String id = textBox1.Text;
                String description = textBox2.Text;
                String address = textBox4.Text;
                String driverType = (string) comboBox1.SelectedItem;

                SimulationDriver driver = new SimulationDriver();
                switch (driverType)
                {
                    case "Sine":
                        driver.TypeSignal = TypeSignal.Sine;
                        break;
                    case "Cosine":
                        driver.TypeSignal = TypeSignal.Cosine;
                        break;
                    case "Triangle":
                        driver.TypeSignal = TypeSignal.Triangle;
                        break;
                    case "Ramp":
                        driver.TypeSignal = TypeSignal.Ramp;
                        break;
                    case "Rectangle":
                        driver.TypeSignal = TypeSignal.Rectangle;
                        break;
                    default:
                        driver.TypeSignal = TypeSignal.Sine;
                        break;
                }
                double initValue = double.Parse(textBox3.Text);
                double lowLimit = double.Parse(textBox7.Text);
                double highLimit = double.Parse(textBox6.Text);
                string units = textBox5.Text;

                this.aoTag = new AO(id, description, driver, address, initValue, lowLimit, highLimit, units);
            }
            catch (Exception exp)
            {
                MessageBox.Show("Wrong inputs!");
                this.DialogResult = DialogResult.Retry;
            }
            
            
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
