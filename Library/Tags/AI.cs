﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScadaLibrary.Tags;
using System.Runtime.Serialization;

namespace ScadaLibrary
{
   [DataContract]
   
   public class AI:InputTag
    {
        [DataMember]
        private double lowLimit;
        [DataMember]
        private double highLimit;
        [DataMember]
        private string units;

        public AI() { }

        public AI(string Id, string Description, SimulationDriver Sd, string Address, int ScanTime, bool Scan, bool Auto,double lowLimit,double highLimit,string units,Alarm alarm)
            : base(Id, Description, Sd, Address, ScanTime, Scan, Auto,alarm)
        {
            this.lowLimit = lowLimit;
            this.highLimit = highLimit;
            this.units = units;
        }

        public string Units
        {
            get { return units; }
            set { units = value; }
        }

        public double HighLimit
        {
            get { return highLimit; }
            set { highLimit = value; }
        }

        public double LowLimit
        {
            get { return lowLimit; }
            set { lowLimit = value; }
        }

        override
         public double calculateValue()
        {

            if (Auto)
            {
                switch (Driver.TypeSignal)
                {
                    case TypeSignal.Sine:
                        return Driver.Sine();
                    case TypeSignal.Cosine:
                        return Driver.Cosine();
                    case TypeSignal.Ramp:
                        return Driver.Ramp();
                    case TypeSignal.Triangle:
                        return Driver.Triangle();
                    case TypeSignal.Rectangle:
                        return Driver.Rectangle();
                    default:
                        return Driver.Sine();
                }
            }
            else
            {
                return Driver.Max/2;
            }
        }
    }
}
