﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ScadaLibrary.Tags
{
    [DataContract]
    public abstract class OutputTag:Tag
    {
        [DataMember]
        private double initialValue;

        public double InitialValue
        {
            get { return initialValue; }
            set { initialValue = value; }
        }

        public OutputTag() { }

        public OutputTag(string Id, string Description, SimulationDriver Sd, string Address,double initValue) : base( Id,  Description,  Sd,  Address) {
            this.initialValue = initValue;
        }
    }
}
