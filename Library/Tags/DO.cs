﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScadaLibrary.Tags;
using System.Runtime.Serialization;


namespace ScadaLibrary
{
    [DataContract]
   public class DO:OutputTag
    {

        public DO(string Id, string Description, SimulationDriver Sd, string Address, double initValue) : base(Id, Description, Sd, Address,initValue) { }


        public DO() { }
        
    }
}
