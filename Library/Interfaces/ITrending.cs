﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScadaLibrary.Tags;
using System.ServiceModel;

namespace ScadaLibrary.Interfaces
{
    [ServiceContract]
    [ServiceKnownType(typeof(Tag))]
    [ServiceKnownType(typeof(DI))]
    [ServiceKnownType(typeof(DO))]
    [ServiceKnownType(typeof(AI))]
    [ServiceKnownType(typeof(AO))]
    public interface ITrending
    {
        [OperationContract]
        Dictionary<string, Tag> getScanTags();
    }
}
