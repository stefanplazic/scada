﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace ScadaLibrary.Interfaces
{

    [ServiceContract]
    [ServiceKnownType(typeof(Alarm))]
    public interface IAlarm
    {
        [OperationContract]
        Dictionary<string, Alarm> getAlarms();
    }
}
