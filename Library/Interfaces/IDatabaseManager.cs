﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScadaLibrary.Tags;
using System.ServiceModel;

namespace ScadaLibrary.Interfaces
{
    [ServiceContract]
    [ServiceKnownType(typeof(Tag))]
    [ServiceKnownType(typeof(DI))]
    [ServiceKnownType(typeof(AI))]
    [ServiceKnownType(typeof(AO))]
    [ServiceKnownType(typeof(DO))]
    public interface IDatabaseManager
    {
        [OperationContract]
        Tag addTag(Tag tag);
        [OperationContract]
        void removeTag(string id);
        [OperationContract]
        Dictionary<string,Tag> getTags();
        
    }
}
