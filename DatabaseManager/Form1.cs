﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScadaLibrary.Interfaces;
using ScadaLibrary;
using ScadaLibrary.Tags;

namespace DatabaseManager
{
    public partial class Form1 : Form
    {
        private IDatabaseManager proxy;

        private DataTable table;

        public Form1(IDatabaseManager proxy)
        {
            InitializeComponent();
            
            this.proxy = proxy;

            table = new DataTable();
            table.Columns.Add("Tag Name");
            table.Columns.Add("Type");
            table.Columns.Add("Description");
            table.Columns.Add("Driver");
            table.Columns.Add("Address");

            Dictionary<String, Tag> dict = proxy.getTags();
            foreach (Tag tag in dict.Values) {
                if (tag is DI)
                    addTagOnTable("DI", tag);
                else if (tag is DO)
                    addTagOnTable("DO", tag);
                else if (tag is AO)
                    addTagOnTable("AO", tag);
                else
                    addTagOnTable("AI", tag);
            }
        }

        
        private void addTagOnTable(String type, Tag tag){
            //table.Clear();
            object[] o = { tag.Id, type, tag.Description, tag.Driver.TypeSignal.ToString(), tag.Adress };
            table.Rows.Add(o);
            dataGridView1.DataSource = table;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked) {

                //start the DIForm
                DIForm dform = new DIForm();
                
               if (dform.ShowDialog() == DialogResult.OK)
                {
                    DI diTag = dform.DiTag;
                    if (proxy.addTag(diTag) == null)
                    {
                        MessageBox.Show("Tag with id " + diTag.Id + " already exist!");
                    }
                    else {

                        MessageBox.Show("Di tag added");
                        addTagOnTable("DI", diTag);
                    }
                   
                    
                }
            }

            /*AI dialog*/
            if (radioButton2.Checked)
            {

                //start the DIForm
                AIForm aform = new AIForm();

                if (aform.ShowDialog() == DialogResult.OK)
                {
                    AI aiTag = aform.AiTag;
                    if (proxy.addTag(aiTag) == null)
                    {
                        MessageBox.Show("Tag with id " + aiTag.Id + " already exist!");
                    }
                    else
                    {

                        MessageBox.Show("Ai tag added");
                        addTagOnTable("AI", aiTag);
                    }
                }
            }

            //DO
            if (radioButton4.Checked)
            {

                //start the DIForm
                DoForm doform = new DoForm();

                if (doform.ShowDialog() == DialogResult.OK)
                {
                    DO doTag = doform.DoTag;
                    if (proxy.addTag(doTag) == null)
                    {
                        MessageBox.Show("Tag with id " + doTag.Id + " already exist!");
                    }
                    else
                    {

                        MessageBox.Show("Do tag added");
                        addTagOnTable("DO", doTag);
                    }
                }
            }

            //AO
            if (radioButton3.Checked)
            {

                //start the DIForm
                AoForm aoform = new AoForm();

                if (aoform.ShowDialog() == DialogResult.OK)
                {
                    AO aoTag = aoform.AoTag;
                    if (proxy.addTag(aoTag) == null)
                    {
                        MessageBox.Show("Tag with id " + aoTag.Id + " already exist!");
                    }
                    else
                    {

                        MessageBox.Show("Ao tag added");
                        addTagOnTable("AO", aoTag);
                    }
                }
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int i = dataGridView1.CurrentCell.RowIndex;
            DataRow dr = table.Rows[i];
            proxy.removeTag(dr["Tag Name"].ToString());
            dr.Delete();
            MessageBox.Show("Tag deleted");
        }

       
    }
}
