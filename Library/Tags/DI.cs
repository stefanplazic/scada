﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ScadaLibrary.Tags;
using System.Runtime.Serialization;


namespace ScadaLibrary
{
    
    [DataContract]
    public class DI:InputTag
    {

        public DI(string Id, string Description,
            SimulationDriver Sd, string Address, 
            int ScanTime, bool Scan, bool Auto, Alarm alarm):base(Id,Description,Sd, Address,ScanTime,Scan,Auto,alarm) { }

        public DI() { }

        override
        public double calculateValue() {

            if (Auto) {
                switch (Driver.TypeSignal)
                {
                    case TypeSignal.Sine:
                        return Driver.Sine();
                    case TypeSignal.Cosine:
                        return Driver.Cosine();
                    case TypeSignal.Ramp:
                        return Driver.Ramp();
                    case TypeSignal.Triangle:
                        return Driver.Triangle();
                    case TypeSignal.Rectangle:
                        return Driver.Rectangle();
                    default:
                        return Driver.Sine();
                }
            }else{
               return Driver.Max/2;
                }
            
            

        }
    }
}
