This project represent a SCAD (Supervisory control and data acquisition) system.  This is the simplified version of system.

***HOW IT WORKS***

There are three independent parts of this project. Each one of them can be deployed on different PC.

- DatabaseManager is component witch implements IDatabaseManager interface and it's represents the most important aspect of this project. It posses method for adding tags to server.(Tag can be digitalinput, digitaloutput, analogInput, analogOutput). After adding tags manager takes care of this signals by modifinig their values depending on what type of signals are they(sine, cosine, ramp, triangle, rectangle). User can also put alarm on certain signals and when their values are  not inside intervals defined in alarm user shall be notified.

-Alarm implements IAlarm interface and enables user to see all signals witch values are outside of alarm defined.

- Trending window implements ITrending intreface which shows graph in witch user can choose what signal to see. After choosing signals user clicks start button and can see the changing values of signals on graph.

***TECHNOLOGY******

Here C# language is used and as for GUI we used Windows Forms.
For this project we used WCF (Windows Communication Foundation) Service.