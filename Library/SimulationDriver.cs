﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ScadaLibrary
{
    public enum TypeSignal { Sine, Cosine, Ramp, Triangle, Rectangle };
  
    [DataContract]
  
    public class SimulationDriver
    {
        [DataMember]
        private double max = 50;
        [DataMember]
        private TypeSignal typeSignal;

        public double Max
        {
            get { return max; }
            set { max = value; }
        }

        public TypeSignal TypeSignal
        {
            get { return typeSignal; }
            set { typeSignal = value; }
        }

        
        public double Sine()
        {
            return max *Math.Sin((double)DateTime.Now.Second / 60 * 2 * Math.PI);

        }

        public double Cosine()
        {

            return max * Math.Cos((double)DateTime.Now.Second / 60 * 2 * Math.PI);

        }

        public double Ramp()
        {

            return max * DateTime.Now.Second / 60;

        }

        
        public double Triangle()
        {
            double v = max * DateTime.Now.Second / 15;

            if (v > max & v <3*max)
                return (2*max - v);
            else if(v>=3*max)
                return (-4*max + v );
            else
                return v;
           
                
        }

        public double Rectangle()
        {
            if (Sine() > 0) return max;
            else
                return -max;
        }
    }
}
